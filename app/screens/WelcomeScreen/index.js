import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Image, View } from 'react-native';
import { Container, Content, Text, Button } from 'native-base';

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: '#673bb7',
    justifyContent: 'flex-end',
  },
  image: {
    width: 250,
    height: 199,
    alignSelf: 'center',
  },
  bottom: {
    paddingTop: 50,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20,
    flexGrow: 0,
    flexBasis: '30%',
    justifyContent: 'flex-end',

  },
  title: {
    fontSize: 20,
    color: '#eaeaea',
  },
  desc: {
    fontSize: 14,
    color: '#c8c8c8',
  },
  next: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 5,
    paddingBottom: 10,

    borderTopColor: '#794fbb',
    borderTopWidth: 1,
  },
  nextButton: {
    alignSelf: 'flex-end',
  },
});

class WelcomeScreen extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <Content {...{ contentContainerStyle: styles.content }}>
          <Image source={require('./assets/welcome.png')} style={styles.image} />

          <View style={styles.bottom}>
            <Text style={styles.title}>Lets Start!</Text>
            <Text style={styles.desc}>Are you still using spreadsheets for company performance monitoring?</Text>
          </View>
          <View style={styles.next}>
            <Button
              style={styles.nextButton}
              transparent
              onPress={() => navigation.navigate('Login')}
            >
              <Text style={{ color: '#fff' }}>Next</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

WelcomeScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};


export default WelcomeScreen;
