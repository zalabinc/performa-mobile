import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  Content,
} from 'native-base';
import {
  View,
  TouchableHighlight,
} from 'react-native';

import perspectives from '../../assets/data/perspectives';
import Header from '../../components/Header';
import Perspective from './components/Perspective';
import Gauge from './components/Gauge';
import Unit from './components/Unit';

class StrategyMapScreen extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <Content>
          <Gauge />
          <Unit {...this.props} />
          <TouchableHighlight onPress={() => navigation.navigate('Perspective')}>
            <View style={{ paddingHorizontal: 17 }}>
              {perspectives.map(data => <Perspective {...this.props} data={data} key={data.id} />)}
            </View>
          </TouchableHighlight>
        </Content>
      </Container>
    );
  }
}

StrategyMapScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

StrategyMapScreen.navigationOptions = ({ navigation }) => ({
  header: (
    <Header {...{ navigation, title: 'Strategy Map' }} />
  ),
});

export default StrategyMapScreen;
