import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Thumbnail,
} from 'native-base';
import {
  View,
  Text,
} from 'react-native';
import Box from '../../../components/Box';

class Perspective extends Component {
  render() {
    const { navigation, data } = this.props;
    return (
      <Box>
        <View style={{ flexDirection: 'row' }}>
          <Thumbnail
            source={require('../../../assets/gauge/gauge2.png')}
            style={{
              marginRight: 10,
            }}
          />
          <View style={{ flex: 1 }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{data.name}</Text>
            <Text style={{ flex: 1 }}>
              {data.description}
            </Text>
            <Text style={{ color: '#0078d7', fontSize: 12, marginTop: 10 }} onPress={() => navigation.navigate('Perspective')}>SEE DETAIL</Text>
          </View>
          <View style={{ alignItems: 'center', marginLeft: 10 }}>
            <Text>Score</Text>
            <Text style={{ fontSize: 28 }}>{data.score}</Text>
            <Thumbnail
              source={require('../../../assets/chart/schedule.png')}
            />
          </View>
        </View>
      </Box>
    );
  }
}

Perspective.propTypes = {
  navigation: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired,
};

export default Perspective;
