import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import {
  Icon,
  Left,
  Body,
  List,
  ListItem,
} from 'native-base';
import {
  View,
  Text,
  Modal,
  TouchableHighlight,
} from 'react-native';

import Box from '../../../components/Box';


const data = [
  { id: 0, name: 'Bank Sumatera Utara' },
  { id: 1, name: 'Bagian Administrasi dan Laporan' },
  { id: 2, name: 'Bidang Akuntansi' },
  { id: 3, name: 'Bidang Bisnis Prosess' },
  { id: 4, name: 'Bidang Dana dan Jasa IB' },
  { id: 5, name: 'Bidang Global Market' },
  { id: 6, name: 'Bidang Infrasturktur IT' },
  { id: 7, name: 'Bidang Intenasional Banking' },
  { id: 8, name: 'Bidang Kebijakan Kredit' },
  { id: 9, name: 'Bidang Kepatuhan' },
];

class StrategyMapScreen extends Component {
  constructor() {
    super();
    this.state = {
      modalVisible: false,
      currentUnitId: 0,
    };
    this.changeUnit = this.changeUnit.bind(this);
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  changeUnit(unitId) {
    this.setState({ currentUnitId: unitId });
    this.setState({ modalVisible: !this.state.modalVisible });
  }

  render() {
    // const { navigation } = this.props;
    return (
      <View style={{ paddingHorizontal: 17 }}>
        <Box>
          <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ flex: 1 }}>
              <Text style={{ fontWeight: 'bold' }}>{data[this.state.currentUnitId].name}</Text>
            </View>
            <TouchableHighlight onPress={() => { this.setModalVisible(!this.state.modalVisible); }}>
              <View>
                <Icon
                  name="arrow-dropdown"
                  style={{ color: '#a6a6a6' }}
                />
              </View>
            </TouchableHighlight>
          </View>
        </Box>


        <View style={{ marginTop: 22 }}>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => { alert('Modal has been closed.'); }}
          >
            <View style={{ margin: 22 }}>
              <View>
                <TouchableHighlight onPress={() => { this.setModalVisible(!this.state.modalVisible); }}>
                  <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontWeight: 'bold' }}>Select Unit</Text>
                    </View>
                    <View>
                      <Icon
                        name="close"
                        style={{ color: '#a6a6a6' }}
                      />
                    </View>
                  </View>
                </TouchableHighlight>
                <List>
                  {data.map(v =>
                    (
                      <ListItem icon key={v.id}>
                        <Left>
                          <Icon name="ios-arrow-round-forward-outline" />
                        </Left>
                        <Body>
                          <TouchableHighlight onPress={() => { this.changeUnit(v.id); }}>
                            <Text>{v.name}</Text>
                          </TouchableHighlight>
                        </Body>
                      </ListItem>
                    ))}
                </List>
              </View>
            </View>
          </Modal>


        </View>
      </View>
    );
  }
}

StrategyMapScreen.propTypes = {
  // navigation: PropTypes.object.isRequired,
};


export default StrategyMapScreen;
