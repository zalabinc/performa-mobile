import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Image } from 'react-native';
import {
  Container,
  Content,
  Text,
  List,
} from 'native-base';

import Header from './components/Header';
import AccountItem from './components/AccountItem';


const styles = StyleSheet.create({
  content: {
    backgroundColor: '#047cc6',
    flex: 1,
  },
  top: {
    flexBasis: '50%',
    backgroundColor: '#f5f5f5',
    justifyContent: 'center',
    alignItems: 'center',
  },
  topImage: {
    width: 211,
    height: 129,
  },
  topText: {
    color: '#6e6f6e',
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'center',
    marginTop: 10,
  },
  bottom: {
    flexBasis: '50%',
    backgroundColor: '#fff',
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
  },
});


class LoginScreen extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <Header />
        <Content contentContainerStyle={styles.content}>

          <View style={styles.top}>
            <Image source={require('./assets/logo.png')} />
            <Text style={styles.topText}>
              You can use this app with all your account.
            </Text>
          </View>
          <View style={styles.bottom}>
            <Text style={{ color: '#1e1e1d', fontSize: 20, marginBottom: 10 }}>
              Choose Your Account
            </Text>

            <List>
              <AccountItem icon="person" title="Personal Account" desc="Use your pesonal account" navigation={navigation} />
              <AccountItem icon="disc" title="Corporate Account" desc="Use your corporate account" navigation={navigation} />
            </List>

          </View>
        </Content>
      </Container>
    );
  }
}

LoginScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default LoginScreen;
