import React from 'react';
import {
  Body,
  Header,
  Left,
  Title,
} from 'native-base';


const LoginHeader = () => (
  <Header>
    <Left style={{ flexBasis: 5, flexGrow: 0 }} />
    <Body>
      <Title>Login</Title>
    </Body>
  </Header>
);

LoginHeader.propTypes = {
};

export default LoginHeader;
