import React from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight } from 'react-native';
import { ListItem, Left, Icon, Body, Text } from 'native-base';

const AccountItem = ({ icon, title, desc, navigation }) => (
  <ListItem icon style={{ marginLeft: 0, marginBottom: 30 }}>
    <Left style={{ alignItems: 'center' }}>
      <TouchableHighlight onPress={() => navigation.navigate('StrategyMap')}>
        <Icon name={icon} style={{ color: '#6a6a6a' }} />
      </TouchableHighlight>
    </Left>
    <Body style={{ borderBottomWidth: 0, justifyContent: 'flex-start' }}>
      <TouchableHighlight onPress={() => navigation.navigate('StrategyMap')}>
        <Text style={{ color: '#151514', fontSize: 16 }}>{title}</Text>
      </TouchableHighlight>
      <TouchableHighlight onPress={() => navigation.navigate('StrategyMap')}>
        <Text style={{ color: '#979696', fontSize: 14 }}>{desc}</Text>
      </TouchableHighlight>
    </Body>
  </ListItem>
);

AccountItem.propTypes = {
  icon: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  navigation: PropTypes.object.isRequired,
};

export default AccountItem;
