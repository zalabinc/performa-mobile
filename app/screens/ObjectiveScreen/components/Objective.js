import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Icon,
} from 'native-base';
import {
  Text,
  View,
} from 'react-native';

import data from '../../../assets/data/objectives';

class Objective extends Component {
  render() {
    // const { navigation } = this.props;
    return (
      <View style={{
        backgroundColor: '#fff',
        paddingHorizontal: 17,
        paddingVertical: 10,
      }}
      >
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 1 }}>
            <Text style={{ color: '#a6a6a6' }}>
              {data[0].id}
            </Text>
            <Text style={{ fontSize: 20 }}>
              {data[0].name}
            </Text>
            <Text style={{}}>
              {data[0].description}
            </Text>
          </View>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 17 }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Icon name="code-working" style={{ fontSize: 18, marginRight: 10, color: '#ea4300' }} />
            <Text style={{ color: '#ea4300' }}>
              {data[0].info}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

Objective.propTypes = {
  // data: PropTypes.object.isRequired,
};

export default Objective;
