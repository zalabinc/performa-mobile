import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  ListItem,
  Thumbnail,
  Body,
} from 'native-base';

import {
  Text,
} from 'react-native';


export default class ObjectiveScreen extends Component {
  render() {
    const { navigation, data } = this.props;
    return (

      <ListItem onPress={() => navigation.navigate('Kpi')}>
        <Thumbnail
          size={80}
          source={require('../../../assets/gauge/percent-small.png')}
          style={{ marginRight: 10, alignSelf: 'flex-start' }}
        />
        <Body>
          <Text style={{ color: '#a6a6a6' }}>
            {data.id}
          </Text>
          <Text style={{ fontSize: 18 }}>{data.name}</Text>
          <Text style={{}}>
            {data.description}
          </Text>
          <Text style={{ color: '#0078d7', fontSize: 12, marginTop: 10 }}>SEE DETAIL</Text>
        </Body>
      </ListItem>

    );
  }
}

ObjectiveScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired,
};

