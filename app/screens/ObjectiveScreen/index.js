import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  Content,
  List,
} from 'native-base';

import {
  Text,
} from 'react-native';

import Header from '../../components/Header';

import Objective from './components/Objective';
import Kpi from './components/Kpi';
import data from '../../assets/data/kpis';

class ObjectiveScreen extends Component {
  render() {
    return (
      <Container style={{ backgroundColor: '#fff' }}>
        <Content>
          <Objective {...this.props} />
          <Text style={{ padding: 17, backgroundColor: '#f4f4f4', fontWeight: 'bold' }}>
              KEY PERFORMANCE INDICATOR
          </Text>
          <List>
            {data.map(value => <Kpi key={value.id} data={value} {...this.props} />)}
          </List>
        </Content>
      </Container>
    );
  }
}

ObjectiveScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};


ObjectiveScreen.navigationOptions = ({ navigation }) => ({
  header: (
    <Header {...{ navigation, title: 'Objective' }} back />
  ),
});

export default ObjectiveScreen;
