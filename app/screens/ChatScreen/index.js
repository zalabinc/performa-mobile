import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView } from 'react-native';
import {
  Container,
  View,
  Text,
  Item,
  Input,
  Icon,
  Right,
  Thumbnail,

} from 'native-base';

import Header from '../../components/Header';


const HeaderRight = () => (
  <Right>
    <Thumbnail source={require('../../assets/users/user06.jpg')} small />
  </Right>
);


class ChatScreen extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <Header {...{ navigation, icon: 'arrow-back', title: 'Jack Morgan', headerRight: <HeaderRight /> }} />

        <View style={{ flex: 1, justifyContent: 'flex-end' }}>

          <View style={{ paddingBottom: 10 }}>
            <ScrollView>

              <View style={{ flexDirection: 'row', paddingHorizontal: 17, marginBottom: 10 }}>
                <Thumbnail
                  source={require('../../assets/users/user04.jpg')}
                  style={{ marginRight: 10, width: 30, height: 30 }}
                />
                <Text style={{ flexWrap: 'wrap', flex: 1, backgroundColor: '#fff', padding: 10, borderRadius: 10 }}>
                  Lorem ipsum dolor
                </Text>
              </View>

              <View style={{ flexDirection: 'row', paddingHorizontal: 10 }}>
                <Text style={{ flexWrap: 'wrap', backgroundColor: '#c8c8c8', padding: 10, borderRadius: 10 }}>
                  Lorem ipsum dolor sit amet consectetur
                  Lorem ipsum dolor sit amet consectetur
                  Lorem ipsum dolor sit amet consectetur
                </Text>
              </View>

            </ScrollView>
          </View>

          <View>
            <Item style={{ backgroundColor: '#fff', marginLeft: 0, paddingHorizontal: 17 }}>
              <Icon name="ios-attach-outline" />
              <Input placeholder="type message" style={{}} />
              <Icon name="ios-paper-plane-outline" />
            </Item>
          </View>
        </View>
      </Container>
    );
  }
}

ChatScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};


export default ChatScreen;
