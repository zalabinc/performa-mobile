import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Body, Content, Text, Card, CardItem } from 'native-base';

import Header from '../../components/Header';


class DashboardScreen extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <Header {...{ navigation, title: 'Dashboard' }} />
        <Content padder>
          <Card>
            <CardItem>
              <Body>
                <Text>
                  Lorem ipsum dolor, sit amet consectetur adipisicing elit. Veniam molestias aliquam et tempora nam illo aspernatur maiores nobis, libero veritatis.
                </Text>
              </Body>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}

DashboardScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default DashboardScreen;
