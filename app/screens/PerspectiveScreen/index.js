import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  Tab,
  Tabs,
  Content,
} from 'native-base';
import {
} from 'react-native';

import Header from '../../components/Header';
import MainTab from './tabs/MainTab';
import ObjectiveTab from './tabs/ObjectiveTab';


class PerspectiveScreen extends Component {
  render() {
    return (
      <Container>
        <Tabs initialPage={0}>
          <Tab heading="Info" style={{ backgroundColor: '#eaeaea' }}>
            <Content>
              <MainTab />
            </Content>
          </Tab>
          <Tab heading="Objectives" style={{ backgroundColor: '#eaeaea' }}>
            <Content>
              <ObjectiveTab {...this.props} />
            </Content>
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

PerspectiveScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

PerspectiveScreen.navigationOptions = ({ navigation }) => ({
  header: (
    <Header {...{ navigation, title: 'Perspective' }} back />
  ),
});
export default PerspectiveScreen;
