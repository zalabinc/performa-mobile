import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Icon,
} from 'native-base';
import {
  Text,
  View,
} from 'react-native';

import Box from '../../../components/Box';

class Objective extends Component {
  render() {
    const { navigation, data } = this.props;
    return (
      <Box>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 1 }}>
            <Text style={{ color: '#a6a6a6' }}>
              {data.id}
            </Text>
            <Text style={{ fontSize: 20 }}>
              {data.name}
            </Text>
            <Text style={{}}>
              {data.description}
            </Text>
          </View>
          <View>
            <Icon
              name="locate"
              style={{ color: '#ea4300' }}
            />
          </View>
        </View>
        <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
          <Icon name="code-working" style={{ fontSize: 18, marginRight: 10, color: '#ea4300' }} />
          <Text style={{ color: '#ea4300' }}>
            {data.info}
          </Text>
        </View>
        <Text style={{ color: '#0078d7', marginTop: 10 }} onPress={() => navigation.navigate('Objective')}>SEE DETAIL</Text>
      </Box>
    );
  }
}

Objective.propTypes = {
  navigation: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired,
};

export default Objective;
