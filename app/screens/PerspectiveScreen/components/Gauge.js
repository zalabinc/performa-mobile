import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import {
} from 'native-base';
import {
  Image,
  Dimensions,
} from 'react-native';


const dimension = Dimensions.get('window');

class Gauge extends Component {
  render() {
    return (
      <Image
        source={require('../../../assets/gauge/gauge3.png')}
        style={{
            width: dimension.width - 50,
            height: 200,
            alignSelf: 'center',
            marginTop: 20,
          }}
        resizeMode="contain"
      />
    );
  }
}

Gauge.propTypes = {
};

export default Gauge;
