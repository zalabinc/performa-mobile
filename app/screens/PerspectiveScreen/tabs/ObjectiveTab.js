import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import {
  View,
} from 'react-native';

import Objective from '../components/Objective';
import data from '../../../assets/data/objectives';

class PerspectiveScreen extends Component {
  render() {
    // const { navigation } = this.props;
    return (
      <View style={{ paddingHorizontal: 17, paddingVertical: 10, backgroundColor: '#f4f4f4' }}>
        {data.map(value => <Objective key={value.id} data={value} {...this.props} />)}
      </View>
    );
  }
}

PerspectiveScreen.propTypes = {
};

export default PerspectiveScreen;
