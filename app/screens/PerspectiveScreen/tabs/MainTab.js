import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import {
} from 'native-base';
import {
  View,
  Text,
} from 'react-native';

import Gauge from '../components/Gauge';
import Box from '../../../components/Box';
import data from '../../../assets/data/perspectives';

class MainTab extends Component {
  render() {
    return (
      <View style={{ paddingHorizontal: 17, backgroundColor: '#eaeaea' }}>
        <Gauge />
        <Box>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 1 }}>
              <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{data[0].name}</Text>
              <Text style={{ flex: 1 }}>
                {data[0].description}
              </Text>
            </View>
            <View style={{ alignItems: 'center', marginLeft: 10 }}>
              <Text>Score</Text>
              <Text style={{ fontSize: 28 }}>{data[0].score}</Text>
            </View>
          </View>
        </Box>
        {/*
        <Box>
          <View style={{ flexDirection: 'row', flex: 1 }}>
            <View style={{ flex: 1 }}>
              <Text style={{ fontWeight: 'bold' }}>Detail Description</Text>
              <Text style={{ flex: 1 }}>
                {data[0].description}
                {data[1].description}
                {data[2].description}
                {data[3].description}
              </Text>
            </View>
          </View>
        </Box>
        */}
      </View>
    );
  }
}

MainTab.propTypes = {
};

export default MainTab;
