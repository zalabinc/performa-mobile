import React from 'react';
import T from 'prop-types';
import {
  Container,
  Body,
  Content,
  Text,
  List,
  ListItem,
  Left,
  Icon,
  Right,
  Switch,
} from 'native-base';

import Header from '../../components/Header';


export default class SettingScreen extends React.Component {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <Header {...{ navigation, title: 'Setting' }}/>
        <Content>
          <List {...{ style: { backgroundColor: '#fff' } }}>

            <ListItem itemDivider icon >
              <Left>
                <Icon {...{ name: 'ios-contact', style: { color: '#3c3c3c' } }} />
              </Left>
              <Body style={{ borderBottomWidth: 0 }}>
                <Text>Account Setting</Text>
              </Body>
            </ListItem>
            <ListItem style={{ marginLeft: 0, paddingLeft: 10 }}>
              <Body>
                <Text>Login with Email</Text>
              </Body>
              <Right>
                <Switch />
              </Right>
            </ListItem>
            <ListItem style={{ marginLeft: 0, paddingLeft: 10 }}>
              <Body>
                <Text>PIN Login</Text>
              </Body>
              <Right>
                <Switch {...{ value: true }} />
              </Right>
            </ListItem>
            <ListItem style={{ marginLeft: 0, paddingLeft: 10 }}>
              <Body style={{ borderBottomWidth: 0 }}>
                <Text>SSO Security</Text>
              </Body>
              <Right style={{ borderBottomWidth: 0 }}>
                <Switch />
              </Right>
            </ListItem>

            <ListItem itemDivider icon>
              <Left>
                <Icon {...{ name: 'cloud-outline', style: { color: '#3c3c3c' } }} />
              </Left>
              <Body style={{ borderBottomWidth: 0 }}>
                <Text>Cloud Options</Text>
              </Body>
            </ListItem>
            <ListItem style={{ marginLeft: 0, paddingLeft: 10 }}>
              <Body>
                <Text>Auto Sync Mode</Text>
              </Body>
              <Right>
                <Switch />
              </Right>
            </ListItem>
            <ListItem style={{ marginLeft: 0, paddingLeft: 10 }}>
              <Body>
                <Text>Backup Account</Text>
              </Body>
              <Right>
                <Switch />
              </Right>
            </ListItem>
            <ListItem style={{ marginLeft: 0, paddingLeft: 10 }}>
              <Body style={{ borderBottomWidth: 0 }}>
                <Text>Semantic Server</Text>
              </Body>
              <Right style={{ borderBottomWidth: 0 }}>
                <Switch />
              </Right>
            </ListItem>

            <ListItem itemDivider icon>
              <Left>
                <Icon {...{ name: 'git-compare', style: { color: '#3c3c3c' } }} />
              </Left>
              <Body style={{ borderBottomWidth: 0 }}>
                <Text>Document</Text>
              </Body>
            </ListItem>
            <ListItem style={{ marginLeft: 0, paddingLeft: 10 }}>
              <Body>
                <Text>Personal Folder</Text>
              </Body>
              <Right>
                <Switch {...{ value: true }} />
              </Right>
            </ListItem>
            <ListItem style={{ marginLeft: 0, paddingLeft: 10 }}>
              <Body>
                <Text>Limit Files</Text>
              </Body>
              <Right>
                <Switch />
              </Right>
            </ListItem>
            <ListItem style={{ marginLeft: 0, paddingLeft: 10 }}>
              <Body style={{ borderBottomWidth: 0 }}>
                <Text>Delay Update</Text>
              </Body>
              <Right style={{ borderBottomWidth: 0 }}>
                <Switch />
              </Right>
            </ListItem>

            <ListItem itemDivider icon>
              <Left>
                <Icon {...{ name: 'copy', style: { color: '#3c3c3c' } }} />
              </Left>
              <Body style={{ borderBottomWidth: 0 }}>
                <Text>Session Mode</Text>
              </Body>
            </ListItem>
            <ListItem style={{ marginLeft: 0, paddingLeft: 10 }}>
              <Body style={{ borderBottomWidth: 0 }}>
                <Text>Direct Access</Text>
              </Body>
              <Right style={{ borderBottomWidth: 0 }}>
                <Switch />
              </Right>
            </ListItem>

          </List>
        </Content>
      </Container>
    );
  }
}

SettingScreen.propTypes = {
  navigation: T.object.isRequired,
};

