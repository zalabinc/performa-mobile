import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
} from 'native-base';
import {
  Text,
  View,
} from 'react-native';


import data from '../../../assets/data/initiatives';


class Objective extends Component {
  render() {
    return (
      <View style={{
        backgroundColor: '#fff',
        paddingHorizontal: 17,
        paddingVertical: 10,
        marginBottom: 10,
      }}
      >
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 1 }}>
            <Text style={{ color: '#a6a6a6' }}>
              {data[0].id}
            </Text>
            <Text style={{ fontSize: 20 }}>
              {data[0].name}
            </Text>
            <Text style={{ color: '#107c10', marginTop: 17 }}>
              Success Measure
            </Text>
            <Text style={{}}>
              {data[0].measure}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

Objective.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Objective;
