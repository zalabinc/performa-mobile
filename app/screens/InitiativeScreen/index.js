import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  Content,
} from 'native-base';

import {
  Text,
  View,
} from 'react-native';

import Header from '../../components/Header';

import Objective from './components/Objective';
import ActionPlan from './components/ActionPlan';

import data from '../../assets/data/actionplan';

class InitiativeScreen extends Component {
  render() {
    return (
      <Container style={{ backgroundColor: '#fff' }}>
        <Content>
          <Objective {...this.props} />
          <Text style={{ padding: 17, backgroundColor: '#ea4300', color: '#fff', fontWeight: 'bold' }}>
            ACTION PLAN
          </Text>
          <View>
            {data.map(value => <ActionPlan key={value.id} data={value} {...this.props} />)}
          </View>
        </Content>
      </Container>
    );
  }
}

InitiativeScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

InitiativeScreen.navigationOptions = ({ navigation }) => ({
  header: (
    <Header {...{ navigation, title: 'Initiative' }} back />
  ),
});
export default InitiativeScreen;
