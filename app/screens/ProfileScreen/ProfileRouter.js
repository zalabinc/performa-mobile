import React from 'react';
import { StackNavigator } from 'react-navigation';

import ProfileScreen from './index';
import EditScreenOne from './EditScreenOne';
import EditScreenTwo from './EditScreenTwo';

const ProfileRouter = StackNavigator({
  Profile: { screen: ProfileScreen },
  EditScreenOne: { screen: EditScreenOne },
  EditScreenTwo: { screen: EditScreenTwo },
});

export default ProfileRouter;
