import React from 'react';
import PropTypes from 'prop-types';
import { Container, Content, View } from 'native-base';

import HeaderPeople from './components/HeaderPeople';
import Box from './components/Box';

class PeopleScreen extends React.Component {
  render() {
    return (
      <Container style={{ backgroundColor: '#fff' }}>
        <HeaderPeople {...this.props} />
        <Content>
          <View style={{ paddingVertical: 17, backgroundColor: '#fff' }}>
            <Box {...this.props} />
            <Box {...this.props} />
            <Box {...this.props} />
            <Box {...this.props} />
            <Box {...this.props} />
            <Box {...this.props} />
          </View>
        </Content>
      </Container>
    );
  }
}

PeopleScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default PeopleScreen;
