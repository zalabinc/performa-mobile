import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  Header,
  Item,
  Icon,
  Input,
  Button,
} from 'native-base';


class HeaderPeople extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <Header searchBar hasTabs rounded>
        <Item>
          <Icon name="menu" onPress={() => navigation.navigate('DrawerOpen')} />
          <Input placeholder="Find People" style={{ margin: 0, padding: 0 }} />
          <Icon name="funnel" />
        </Item>
        <Button transparent>
          <Text>Search</Text>
        </Button>
      </Header>
    );
  }
}

HeaderPeople.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default HeaderPeople;
