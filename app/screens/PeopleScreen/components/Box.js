import React from 'react';
import PropTypes from 'prop-types';
import { TouchableHighlight } from 'react-native';
import { Text, View, Thumbnail, Icon } from 'native-base';


class PeopleScreen extends React.Component {
  render() {
    return (
      <View style={{
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomColor: '#f4f4f4',
        borderBottomWidth: 1,
        paddingHorizontal: 17,
        paddingVertical: 10,
      }}
      >
        <Thumbnail source={require('../../../assets/users/user01.jpg')} small style={{ marginRight: 10 }} />
        <View style={{ flex: 1 }}>
          <Text style={{ lineHeight: 16, color: '#333333' }}>Robert Gordon</Text>
          <Text style={{ fontSize: 14, color: '#666666' }}>Marketing Division</Text>
        </View>
        <TouchableHighlight onPress={() => this.props.navigation.navigate('Chat')}>
          <View style={{ flexDirection: 'row' }}>
            <Icon name="call" style={{ fontSize: 25, color: '#0078d7' }} />
            <Icon name="mail" style={{ fontSize: 25, marginLeft: 20, color: '#666666' }} />
          </View>
        </TouchableHighlight>
      </View >
    );
  }
}

PeopleScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
}

export default PeopleScreen;
