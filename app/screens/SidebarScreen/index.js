import React from 'react';
import T from 'prop-types';

import {
  Container,
  Content,
} from 'native-base';
import {
  View,
  Text,
} from 'react-native';
// import MenuList from './components/MenuList';
import MenuListMain from './components/MenuListMain';
import TopSidebar from './components/TopSidebar';

class SideBarScreen extends React.Component {
  render() {
    return (
      <Container>
        <Content {...{ style: { backgroundColor: '#fff' } }}>
          <TopSidebar {...this.props} />
          <MenuListMain {...this.props} />
          {
            /*
            <View style={{ borderBottomColor: '#ddd', borderBottomWidth: 1 }} />
             */
          }


        </Content>
      </Container>
    );
  }
}
SideBarScreen.propTypes = {
  navigation: T.object.isRequired,
};
export default SideBarScreen;
