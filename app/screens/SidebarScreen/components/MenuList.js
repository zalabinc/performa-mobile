import React from 'react';
import T from 'prop-types';

import {
  Text,
  List,
  ListItem,
  Icon,
  Left,
  Body,
} from 'native-base';

const routes = [
  { title: 'Home', screen: 'Home', icon: 'home' },
  { title: 'Profile', screen: 'Profile', icon: 'contact' },
  { title: 'Company', screen: 'Company', icon: 'globe' },
  { title: 'Dashboard', screen: 'Dashboard', icon: 'pie' },
  { title: 'Strategy Map', screen: 'StrategyMap', icon: 'git-merge' },
  { title: 'Message', screen: 'Message', icon: 'mail' },
  { title: 'Chat', screen: 'Chat', icon: 'chatbubbles' },
  { title: 'People', screen: 'People', icon: 'contacts' },
  { title: 'ScoreCard', screen: 'ScoreCard', icon: 'speedometer' },
  { title: 'Setting', screen: 'Setting', icon: 'construct' },
  { title: 'Perspective', screen: 'Perspective', icon: 'body' },
  { title: 'Objective', screen: 'Objective', icon: 'paper' },
  { title: 'Key Performa', screen: 'Kpi', icon: 'ribbon' },
  { title: 'Initiative', screen: 'Initiative', icon: 'bulb' },
  { title: 'Action Plan', screen: 'ActionPlan', icon: 'bicycle' },
  { title: 'Welcome', screen: 'Welcome', icon: 'ios-remove-outline' },
  { title: 'Login', screen: 'Login', icon: 'ios-remove-outline' },
];


class SideBarScreen extends React.Component {
  render() {
    return (
      <List
        {...{
        dataArray: routes,
        style: {
          marginTop: 20,
        },
      }}
        renderRow={data => (
          <ListItem
            button
            icon
            {...{
            onPress: () => this.props.navigation.navigate(data.screen),
            style: {
              backgroundColor: 'transparent',
              marginBottom: 10,
            },
          }}
          >
            <Left>
              <Icon {...{ name: data.icon, style: { color: '#a6a6a6' } }} />
            </Left>
            <Body {...{ style: { borderBottomWidth: 0 } }}>
              <Text style={{ color: '#212121', marginLeft: 10 }} >{data.title}
              </Text>
            </Body>
          </ListItem>
      )}
      />
    );
  }
}
SideBarScreen.propTypes = {
  navigation: T.object.isRequired,
};
export default SideBarScreen;
