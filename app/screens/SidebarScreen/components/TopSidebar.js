import React from 'react';
import T from 'prop-types';

import {
  Text,
  Thumbnail,
} from 'native-base';
import { Image } from 'react-native';


const ImageBg = require('../assets/profile_bg.jpg');
const ImageProfile = require('../assets/user.jpg');

class SideBarScreen extends React.Component {
  render() {
    return (
      <Image {...{
            source: ImageBg,
            style: {
              height: 200, justifyContent: 'flex-end', paddingLeft: 20, paddingBottom: 20,
            },
          }}
      >
        <Thumbnail {...{ source: ImageProfile, size: 50, style: { marginBottom: 10 } }} />
        <Text {...{
              style: {
                color: '#fff', fontSize: 18, fontWeight: 'bold', lineHeight: 16,
              },
            }}
        >
              Curtis Barnet
        </Text>
        <Text {...{ style: { color: '#fff', fontSize: 13 } }} >
              Marketing Division
        </Text>
      </Image>


    );
  }
}
SideBarScreen.propTypes = {
  navigation: T.object.isRequired,
};
export default SideBarScreen;
