import React from 'react';
import T from 'prop-types';

import {
  Text,
  List,
  ListItem,
  Icon,
  Left,
  Body,
} from 'native-base';

const routes = [
  { title: 'Strategy Map', screen: 'StrategyMap', icon: 'git-merge' },
  { title: 'Dashboard', screen: 'Dashboard', icon: 'speedometer' },
  { title: 'Message', screen: 'Message', icon: 'mail' },
  { title: 'Chat', screen: 'Chat', icon: 'chatbubbles' },
  { title: 'Profile', screen: 'Profile', icon: 'contact' },
  { title: 'People', screen: 'People', icon: 'contacts' },
  { title: 'Setting', screen: 'Setting', icon: 'construct' },
  { title: 'Logout', screen: 'Login', icon: 'exit' },
];


class SideBarScreen extends React.Component {
  render() {
    return (
      <List
        {...{
        dataArray: routes,
        style: {
          marginTop: 20,
        },
      }}
        renderRow={data => (
          <ListItem
            button
            icon
            {...{
            onPress: () => this.props.navigation.navigate(data.screen),
            style: {
              backgroundColor: 'transparent',
              marginBottom: 10,
            },
          }}
          >
            <Left>
              <Icon {...{ name: data.icon, style: { color: '#a6a6a6' } }} />
            </Left>
            <Body {...{ style: { borderBottomWidth: 0 } }}>
              <Text style={{ color: '#212121', marginLeft: 10 }} >{data.title}
              </Text>
            </Body>
          </ListItem>
      )}
      />
    );
  }
}
SideBarScreen.propTypes = {
  navigation: T.object.isRequired,
};
export default SideBarScreen;
