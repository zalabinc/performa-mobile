import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  Thumbnail,
  Tabs,
  Tab,
} from 'native-base';
import {
  Text,
  View,
} from 'react-native';


import Header from '../../components/Header';
import Score from './components/Score';
import Profile from './components/Profile';
import Chart from './components/Chart';

class UserScreen extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <Header {...{ navigation, title: 'User' }} />
        <View style={{ flex: 1 }}>

          <View style={{ alignItems: 'center', paddingVertical: 20, backgroundColor: '#eaeaea' }}>
            <Thumbnail source={require('../../assets/users/user05.jpg')} large />
            <Text style={{ fontSize: 20 }}>Mr. Amdani Joe</Text>
            <Text style={{ fontSize: 13 }}>Marketing Manager</Text>
          </View>

          <View style={{ borderTopWidth: 1, borderTopColor: '#ddd', flex: 1 }}>
            <Tabs
              initialPage={2}
              tabBarUnderlineStyle={{ backgroundColor: '#ea4300' }}
            >
              <Tab
                heading="Company"
                tabStyle={{ backgroundColor: '#fff' }}
                textStyle={{ color: '#000' }}
                activeTextStyle={{ color: '#ea4300' }}
                activeTabStyle={{ backgroundColor: '#fff' }}
              >
                <Profile />
              </Tab>
              <Tab
                heading="Progress"
                tabStyle={{ backgroundColor: '#fff' }}
                textStyle={{ color: '#000' }}
                activeTextStyle={{ color: '#ea4300' }}
                activeTabStyle={{ backgroundColor: '#fff' }}
              >
                <Chart />
              </Tab>
              <Tab
                heading="Score"
                tabStyle={{ backgroundColor: '#fff' }}
                textStyle={{ color: '#000' }}
                activeTextStyle={{ color: '#ea4300' }}
                activeTabStyle={{ backgroundColor: '#fff' }}
              >
                <Score />
              </Tab>
            </Tabs>
          </View>
        </View>
      </Container>
    );
  }
}

UserScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default UserScreen;
