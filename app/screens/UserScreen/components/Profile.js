import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import {
  View,
  Text,
} from 'native-base';


class Profile extends Component {
  render() {
    return (
      <View style={{ padding: 17, alignItems: 'flex-start' }}>
        <Text style={{ fontSize: 15 }}>About Company</Text>
        <Text style={{ fontSize: 14, marginTop: 10 }}>Vision Statement</Text>
        <Text style={{ fontSize: 13 }}>
          Menjadi Bank andalan bagi membantu dan mendorong pertumbuhan perekonomian dan pembangunan daerah di segala bidang serta sebagai salah satu sumber pendapatan daerah dalam rangka meningkatkan taraf hidup rakyat.
        </Text>
        <Text style={{ fontSize: 14, marginTop: 10 }}>Mission Statement</Text>
        <Text style={{ fontSize: 13 }}>
          Menjadi Bank andalan bagi membantu dan mendorong pertumbuhan perekonomian dan pembangunan daerah di segala bidang serta sebagai salah satu sumber pendapatan daerah dalam rangka meningkatkan taraf hidup rakyat.
        </Text>
        <Text style={{ fontSize: 14, marginTop: 10 }}>Core Value</Text>
        <Text style={{ fontSize: 13 }}>
          Terpercaya - Enerjik - Ramah - Bersahabat - Aman - Integritas Tinggi - Komitmen
        </Text>
      </View>
    );
  }
}

Profile.propTypes = {
};

export default Profile;
