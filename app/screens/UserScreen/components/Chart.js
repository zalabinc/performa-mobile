import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import {
  View,
  Text,
} from 'native-base';
import {
  Image,
} from 'react-native';


class Chart extends Component {
  render() {
    return (
      <View style={{ padding: 17, alignItems: 'center' }}>
        <Text style={{ }}>
          User Performa
        </Text>
        <Image
          source={require('../assets/chart.png')}
          style={{ height: 200, width: 200, alignSelf: 'center' }}
          resizeMode="contain"
        />
      </View>
    );
  }
}

Chart.propTypes = {
};

export default Chart;
