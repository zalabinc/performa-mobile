import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import {
  View,
  Image,
  ScrollView,
} from 'react-native';


class Score extends Component {
  render() {
    return (
      <View style={{ alignItems: 'center', backgroundColor: '#3c3c3c', flex: 1 }}>
        <ScrollView>
          <Image
            source={require('../assets/gauge.png')}
            style={{ height: 300, width: 300, alignSelf: 'center' }}
            resizeMode="contain"
          />
        </ScrollView>
      </View>
    );
  }
}

Score.propTypes = {
};

export default Score;
