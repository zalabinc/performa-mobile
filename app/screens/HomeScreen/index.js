import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Image } from 'react-native';
import { Container, Content, Text, View, Icon, Segment, Button } from 'native-base';
// import Header from '../../components/Header';


// const dimension = Dimensions.get('window');

const styles = StyleSheet.create({
  content: {
    flex: 1,
  },
  container: {
    flex: 1,
    width: undefined,
    height: undefined,
    backgroundColor: 'transparent',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },

});


class HomeScreen extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <Content contentContainerStyle={styles.content}>
          <Image source={require('./assets/bg.jpg')} style={styles.container}>

            <View style={{ alignSelf: 'flex-start' }}>
              <Button transparent onPress={() => navigation.navigate('DrawerOpen')}>
                <Icon name="menu" style={{ color: '#fff' }} />
              </Button>
            </View>

            <Image
              source={require('./assets/gauge2.png')}
              style={{
               width: 250,
               height: 250,
               resizeMode: 'contain',
            }}
            />

            <Segment>
              <Button first>
                <Text>Actual</Text>
              </Button>
              <Button last active>
                <Text>MONTHLY</Text>
              </Button>
            </Segment>

            <View style={{ flexDirection: 'row', paddingTop: 40 }}>
              <Image
                source={require('./assets/gauge3.png')}
                style={{
                  width: 100,
                  height: 100,
                  resizeMode: 'contain',
                }}
              />
              <Image
                source={require('./assets/gauge3.png')}
                style={{
                  width: 100,
                  height: 100,
                  resizeMode: 'contain',
                }}
              />
            </View>

            <Segment>
              <Button first>
                <Text>Week</Text>
              </Button>
              <Button last active>
                <Text>Month</Text>
              </Button>
            </Segment>

          </Image>
        </Content>
      </Container>
    );
  }
}

HomeScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default HomeScreen;
