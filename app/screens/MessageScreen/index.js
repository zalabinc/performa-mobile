import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { Container, Tab, Tabs, TabHeading, Text } from 'native-base';
import Header from './components/HeaderMessage';
import BoxMessage from './components/BoxMessage';


class MessageScreen extends Component {
  render() {
    return (
      <Container>
        <Header {...this.props} />
        <Tabs initialPage={1}>
          <Tab heading="Inbox">
            <BoxMessage />
          </Tab>
          <Tab heading={<TabHeading><Text>Sent</Text></TabHeading>}>
            <BoxMessage />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

MessageScreen.propTypes = {
};

export default MessageScreen;
