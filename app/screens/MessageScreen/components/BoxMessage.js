import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { Body, Content, Text, List, ListItem, Thumbnail, Right,
} from 'native-base';

const user01 = require('../../../assets/users/user01.jpg');

const messages = Object.keys([...Array(10)]);

class BoxMessage extends Component {
  render() {
    return (
      <List
        {...{
              dataArray: messages,
              style: {
                backgroundColor: '#000',
              },
              renderRow: () => (
                <ListItem style={{ alignItems: 'flex-start', marginLeft: 0, paddingLeft: 17 }}>
                  <Thumbnail style={{ width: 35, height: 35 }} source={user01} />
                  <Body>
                    <Text>Sankhadeep</Text>
                    <Text note>Its time to build a difference Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi, aspernatur? </Text>
                  </Body>
                  <Right style={{ alignSelf: 'flex-start' }}>
                    <Text note>02.22</Text>
                  </Right>
                </ListItem>
              ),

            }}
      />
    );
  }
}

BoxMessage.propTypes = {
};

export default BoxMessage;
