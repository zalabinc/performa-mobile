import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  Content,
  Item,
  Input,
  Icon,
} from 'native-base';

import {
  Text,
  View,
} from 'react-native';

import Header from '../../components/Header';
import ActionPlan from './components/ActionPlan';
import Chat from './components/Chat';

import data from '../../assets/data/actionplan';

class ActionPlanScreen extends Component {
  render() {
    return (
      <Container style={{ backgroundColor: '#fff' }}>
        <Text style={{ padding: 17, backgroundColor: '#ea4300', color: '#fff', fontWeight: 'bold' }}>
          DISCUSSION GROUP
        </Text>
        <Content>
          <ActionPlan data={data} {...this.props} />
          <Chat {...this.props} />
        </Content>
        <View>
          <Item style={{ backgroundColor: '#fff', marginLeft: 0, paddingHorizontal: 17 }}>
            <Icon name="ios-attach-outline" />
            <Input placeholder="type message" style={{}} />
            <Icon name="ios-paper-plane-outline" />
          </Item>
        </View>
      </Container>
    );
  }
}

ActionPlanScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

ActionPlanScreen.navigationOptions = ({ navigation }) => ({
  header: (
    <Header {...{ navigation, title: 'Action Plan' }} back />
  ),
});


export default ActionPlanScreen;
