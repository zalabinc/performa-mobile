import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  Thumbnail,

} from 'native-base';


class ChatScreen extends Component {
  render() {
    // const { navigation } = this.props;
    return (

      <View style={{ flex: 1, justifyContent: 'flex-end', backgroundColor: '#eaeaea', paddingTop: 10 }}>
        <View style={{ paddingBottom: 10 }}>

          <View style={{ flexDirection: 'row', paddingHorizontal: 17, marginBottom: 10 }}>
            <Thumbnail
              source={require('../../../assets/users/user04.jpg')}
              style={{ marginRight: 10, width: 30, height: 30 }}
            />
            <Text style={{ flexWrap: 'wrap', flex: 1, backgroundColor: '#fff', padding: 10, borderRadius: 10 }}>
                  Document terbaru tentang CKPN terbaru apakah sudah dikirim ?
            </Text>
          </View>

          <View style={{ flexDirection: 'row', paddingHorizontal: 17, marginBottom: 10 }}>
            <Thumbnail
              source={require('../../../assets/users/user07.jpg')}
              style={{ marginRight: 10, width: 30, height: 30 }}
            />
            <Text style={{ flexWrap: 'wrap', flex: 1, backgroundColor: '#fff', padding: 10, borderRadius: 10 }}>
              Sudah Coba tanyakan langsung ke bu diah ?
            </Text>
          </View>

          <View style={{ flexDirection: 'row', paddingHorizontal: 17, marginBottom: 10 }}>
            <Thumbnail
              source={require('../../../assets/users/user04.jpg')}
              style={{ marginRight: 10, width: 30, height: 30 }}
            />
            <Text style={{ flexWrap: 'wrap', flex: 1, backgroundColor: '#fff', padding: 10, borderRadius: 10 }}>
                Oh.. ternyata sudah ada dalam cloud divisi....
            </Text>
          </View>

          <View style={{ flexDirection: 'row', paddingHorizontal: 17, marginBottom: 10 }}>
            <Thumbnail
              source={require('../../../assets/users/user09.jpg')}
              style={{ marginRight: 10, width: 30, height: 30 }}
            />
            <Text style={{ flexWrap: 'wrap', flex: 1, backgroundColor: '#fff', padding: 10, borderRadius: 10 }}>
              Ok....
            </Text>
          </View>


        </View>
      </View>
    );
  }
}

ChatScreen.propTypes = {
  // navigation: PropTypes.object.isRequired,
};


export default ChatScreen;
