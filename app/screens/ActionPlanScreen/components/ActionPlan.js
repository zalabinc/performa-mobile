import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import { Thumbnail, Icon } from 'native-base';

class Box extends Component {
  render() {
    const { data } = this.props;
    return (
      <View style={{

        paddingVertical: 17,
        paddingHorizontal: 17,
        borderBottomWidth: 1,
        borderBottomColor: '#c8c8c8',

      }}
      >

        {/* TOP */}
        <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
          <View style={{ flex: 1 }}>
            <Text style={{ color: '#a6a6a6' }}>
              Action Plan {data[0].id}
            </Text>
            <Text style={{ fontSize: 18 }}>
              {data[0].name}
            </Text>
          </View>
          <View style={{}}>
            <Icon name="bicycle" style={{ fontSize: 32, color: '#ea4300' }} />
          </View>
        </View>


        <View style={{ paddingBottom: 10 }}>
          <View style={{ paddingBottom: 10 }}>
            <Text>Assigned to</Text>
          </View>
          <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
            <Thumbnail source={require('../../../assets/users/user01.jpg')} small style={{ marginRight: 5, marginBottom: 5 }} />
            <Thumbnail source={require('../../../assets/users/user02.jpg')} small style={{ marginRight: 5, marginBottom: 5 }} />
            <Thumbnail source={require('../../../assets/users/user03.jpg')} small style={{ marginRight: 5, marginBottom: 5 }} />
            <Thumbnail source={require('../../../assets/users/user04.jpg')} small style={{ marginRight: 5, marginBottom: 5 }} />
            <Thumbnail source={require('../../../assets/users/user05.jpg')} small style={{ marginRight: 5, marginBottom: 5 }} />
            <Thumbnail source={require('../../../assets/users/user07.jpg')} small style={{ marginRight: 5, marginBottom: 5 }} />
          </View>
        </View>


      </View>
    );
  }
}

Box.propTypes = {
  // navigation: PropTypes.object.isRequired,
  data: PropTypes.array.isRequired,
};

export default Box;
