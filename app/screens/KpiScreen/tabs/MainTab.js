import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import {
} from 'native-base';
import {
  View,
  Text,
} from 'react-native';

import Chart from '../components/Chart';
import ChartAlt from '../components/ChartAlt';
import data from '../../../assets/data/kpis';

class MainTab extends Component {
  render() {
    return (
      <View style={{}}>
        <View style={{ paddingHorizontal: 17, marginTop: 17 }}>
          <Text style={{ color: '#a6a6a6' }}>
            {data[0].id}
          </Text>
          <Text style={{ fontSize: 20, marginBottom: 10 }}>
            {data[0].name}
          </Text>
          <Text style={{ fontWeight: 'bold' }}>Detail KPI Description</Text>
          <Text style={{ flex: 1 }}>
            {data[0].description}
          </Text>
        </View>
        <Chart />
        <ChartAlt />
      </View>
    );
  }
}

MainTab.propTypes = {
};

export default MainTab;
