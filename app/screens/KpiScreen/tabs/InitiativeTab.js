import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import {
  View,
} from 'react-native';

import Initiative from '../components/Initiative';
import data from '../../../assets/data/initiatives';

class InitiativeTab extends Component {
  render() {
    // const { navigation } = this.props;
    return (
      <View style={{ paddingHorizontal: 17, paddingVertical: 10, backgroundColor: '#f4f4f4' }}>
        {data.map(value => <Initiative key={value.id} data={value} {...this.props} />)}
      </View>
    );
  }
}

InitiativeTab.propTypes = {
};

export default InitiativeTab;
