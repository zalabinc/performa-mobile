import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import {
  View,
} from 'react-native';

import Actual from '../components/Actual';

import data from '../../../assets/data/actual';

class PerspectiveScreen extends Component {
  render() {
    // const { navigation } = this.props;
    return (
      <View style={{ paddingHorizontal: 17, paddingVertical: 10, backgroundColor: '#f4f4f4' }}>
        {data.map(value => <Actual key={value.id} data={value} {...this.props} />)}
      </View>
    );
  }
}

PerspectiveScreen.propTypes = {
};

export default PerspectiveScreen;
