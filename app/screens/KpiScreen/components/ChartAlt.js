import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import {
} from 'native-base';
import {
  Image,
  Dimensions,
  View,
} from 'react-native';


const dimension = Dimensions.get('window');

class Gauge extends Component {
  render() {
    return (
      <View style={{ backgroundColor: '#fff' }}>
        <Image
          source={require('../../../assets/chart/7.jpg')}
          style={{
            width: dimension.width - 50,
            height: 200,
            alignSelf: 'center',
            marginTop: 20,
          }}
          resizeMode="contain"
        />
      </View>
    );
  }
}

Gauge.propTypes = {
};

export default Gauge;
