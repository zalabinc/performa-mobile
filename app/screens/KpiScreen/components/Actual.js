import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Icon,
  Thumbnail,
} from 'native-base';
import {
  Text,
  View,
} from 'react-native';

import Box from '../../../components/Box';

class KpiInfo extends Component {
  render() {
    const { data } = this.props;
    return (
      <Box>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 1 }}>
            <Text style={{ color: '#32145a', textAlign: 'center' }}>
              PERIOD MONTH {data.id}
            </Text>
            <Text style={{ color: '#a6a6a6', textAlign: 'center', fontSize: 12 }}>
              TERGET x ACTUAL
            </Text>

            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
              <Text style={{ fontSize: 30, color: '#5c2d91' }}>
                {data.target}
              </Text>
              <Icon name="ios-shuffle-outline" style={{ marginLeft: 17 }} />
              <Text style={{ fontSize: 30, marginLeft: 17, color: '#a80000' }}>
                {data.actual}
              </Text>
            </View>
          </View>

          <View style={{
            alignItems: 'center',
            justifyContent: 'center',

            marginLeft: 17,
          }}
          >
            <Thumbnail source={require('../../../assets/gauge/gauge2.png')} size={30} />
          </View>
        </View>
        <View style={{
          borderTopColor: '#ddd',
          borderTopWidth: 1,
          marginTop: 17,
          paddingTop: 5,
          flex: 1,
          // backgroundColor: '#eaeaea',
          // marginHorizontal: -17,
          // paddingHorizontal: 17,
          // marginVertical: -17,
          // paddingVertical: -17,
          // paddingBottom: 20,
        }}
        >
          <Text style={{ }}>
            Note
          </Text>
          <Text>
            {data.note}
          </Text>
        </View>
      </Box>
    );
  }
}

KpiInfo.propTypes = {
  data: PropTypes.object.isRequired,
};

export default KpiInfo;
