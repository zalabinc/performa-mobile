import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import { Card, CardItem, Body, Thumbnail, Icon, Button } from 'native-base';

class Box extends Component {
  render() {
    const { data } = this.props;
    return (
      <Card>
        <CardItem>
          <Body>

            {/* TOP */}
            <View style={{ flexDirection: 'row', paddingBottom: 10 }}>
              <View style={{ flex: 1 }}>
                <Text style={{ color: '#a6a6a6' }}>
                  {data.id}
                </Text>
                <Text style={{ fontSize: 18 }}>
                  {data.name}
                </Text>
              </View>
              <View style={{}}>
                <Icon name="bulb" style={{ fontSize: 28, color: '#ea4300' }} />
              </View>
            </View>


            <View style={{ paddingBottom: 10 }}>
              <View style={{ paddingBottom: 10 }}>
                <Text>Assigned to</Text>
              </View>
              <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                <Thumbnail source={require('../../../assets/users/user01.jpg')} small style={{ marginRight: 5, marginBottom: 5 }} />
                <Thumbnail source={require('../../../assets/users/user02.jpg')} small style={{ marginRight: 5, marginBottom: 5 }} />
                <Thumbnail source={require('../../../assets/users/user03.jpg')} small style={{ marginRight: 5, marginBottom: 5 }} />
                <Thumbnail source={require('../../../assets/users/user04.jpg')} small style={{ marginRight: 5, marginBottom: 5 }} />
                <Thumbnail source={require('../../../assets/users/user05.jpg')} small style={{ marginRight: 5, marginBottom: 5 }} />
                <Thumbnail source={require('../../../assets/users/user07.jpg')} small style={{ marginRight: 5, marginBottom: 5 }} />
              </View>
              <Button onPress={() => this.props.navigation.navigate('Initiative')} transparent>
                <Text style={{ color: '#0078d7', fontSize: 14, marginTop: 10 }}>SEE DETAIL</Text>
              </Button>
            </View>


          </Body>
        </CardItem>
      </Card>
    );
  }
}

Box.propTypes = {
  navigation: PropTypes.object.isRequired,
  data: PropTypes.object.isRequired,
};

export default Box;
