import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Icon,
} from 'native-base';
import {
  Text,
  View,
} from 'react-native';

import Box from '../../../components/Box';

class KpiInfo extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <Box>
        <View style={{ flexDirection: 'row' }}>
          <View style={{ flex: 1 }}>
            <Text style={{ color: '#a6a6a6' }}>
              KpiInfo 1
            </Text>
            <Text style={{ fontSize: 20 }}>
              Consultant Management
            </Text>
            <Text style={{}}>
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Iusto, dicta unde fuga pariatur tenetur eos nemo omnis dolore nulla suscipit.
            </Text>
          </View>
          <View>
            <Icon
              name="checkmark"
              style={{ color: '#ea4300' }}
            />
          </View>
        </View>
        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 17 }}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <Icon name="code-working" style={{ fontSize: 18, marginRight: 10, color: '#ea4300' }} />
            <Text style={{ color: '#ea4300' }}>
              3 KPI & 10 Initiative
            </Text>
          </View>
          <Text style={{ color: '#0078d7' }} onPress={() => navigation.navigate('KpiInfo')}>SEE DETAIL</Text>
        </View>
      </Box>
    );
  }
}

KpiInfo.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default KpiInfo;
