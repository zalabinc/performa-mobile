import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Container,
  Tab,
  Tabs,
  Content,
} from 'native-base';
import {
} from 'react-native';

import Header from '../../components/Header';
import MainTab from './tabs/MainTab';
import ActualTab from './tabs/ActualTab';
import InitiativeTab from './tabs/InitiativeTab';


class KpiScreen extends Component {
  render() {
    return (
      <Container>
        <Tabs initialPage={0}>
          <Tab heading="Info" style={{ backgroundColor: '#fff' }}>
            <Content contentContainerStyle={{ paddingBottom: 20 }}>
              <MainTab />
            </Content>
          </Tab>
          <Tab heading="Actual">
            <Content>
              <ActualTab {...this.props} />
            </Content>
          </Tab>
          <Tab heading="Initiative">
            <Content>
              <InitiativeTab {...this.props} />
            </Content>
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

KpiScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

KpiScreen.navigationOptions = ({ navigation }) => ({
  header: (
    <Header {...{ navigation, title: 'KPI' }} back />
  ),
});


export default KpiScreen;
