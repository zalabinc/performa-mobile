import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Modal, View, Dimensions } from 'react-native';
import { Container, Button, Body, Content, Text, Card, CardItem } from 'native-base';
import Header from '../../components/Header';

class CompanyScreen extends Component {
  constructor() {
    super();
    this.state = {
      modalVisible: false,
    };
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  render() {
    const { navigation } = this.props;
    const { height, width } = Dimensions.get('window');
    return (
      <Container>
        <Header {...{ navigation, title: 'Company' }} />
        <Content padder>
          <Card>
            <CardItem>
              <Body>
                <Text>
                  {width} x {height}
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Button onPress={() => {
            this.setModalVisible(true);
          }}
          >
            <Text>Show Modal</Text>
          </Button>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => { alert('Modal has been closed.'); }}
          >
            <View style={{ marginTop: 22, padding: 20 }}>
              <View>
                <Text>Hello World!</Text>

                <Button onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}
                >
                  <Text>Hide Modal</Text>
                </Button>

              </View>
            </View>
          </Modal>
        </Content>
      </Container>
    );
  }
}

CompanyScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default CompanyScreen;
