import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Body, Card, CardItem } from 'native-base';

class Box extends Component {
  render() {
    return (
      <Card>
        <CardItem>
          <Body>
            {this.props.children}
          </Body>
        </CardItem>
      </Card>
    );
  }
}

Box.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Box;
