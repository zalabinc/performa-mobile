import React from 'react';
import PropTypes from 'prop-types';
import { Left, Button, Icon } from 'native-base';

const HeaderLeft = ({ icon, back, navigation, link }) => {
  const iconType = back ? 'arrow-back' : icon;
  return (
    <Left>
      <Button
        {...{
          transparent: true,
          onPress: () => (back ? navigation.goBack() : navigation.navigate(link)),
        }}
      >
        <Icon name={iconType} />
      </Button>
    </Left>
  );
};

HeaderLeft.propTypes = {
  link: PropTypes.string.isRequired,
  back: PropTypes.bool.isRequired,
  icon: PropTypes.string.isRequired,
  navigation: PropTypes.object.isRequired,
};

export default HeaderLeft;
