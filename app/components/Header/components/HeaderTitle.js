import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { Body, Title } from 'native-base';

const styles = StyleSheet.create({
  bodyStyle: {
    // flex: 3,
  },
});

const HeaderTitle = ({ title }) => {
  return (
    <Body style={styles.bodyStyle}>
      <Title>{title}</Title>
    </Body>
  );
}


HeaderTitle.propTypes = {
  title: PropTypes.string.isRequired,
};


export default HeaderTitle;
