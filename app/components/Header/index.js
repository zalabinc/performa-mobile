import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Header,
  Right,
} from 'native-base';

import HeaderTitle from './components/HeaderTitle';
import HeaderLeft from './components/HeaderLeft';

class HeaderComponent extends Component {
  render() {
    const {
      navigation,
      back,
      link,
      icon,
      title,
      headerRight,
      headerLeft,
      headerBody,
    } = this.props;
    return (
      <Header>
        {headerLeft || <HeaderLeft {...{ navigation, back, icon, link }} />}
        {headerBody || <HeaderTitle {...{ title }} />}
        {headerRight || <Right />}
      </Header>
    );
  }
}

HeaderComponent.defaultProps = {
  headerLeft: false,
  headerBody: false,
  headerRight: false,
  title: '',
  icon: 'menu',
  link: 'DrawerOpen',
  back: false,
};
HeaderComponent.propTypes = {
  navigation: PropTypes.object.isRequired,
  icon: PropTypes.string,
  back: PropTypes.bool,
  link: PropTypes.string,
  title: PropTypes.string,
  headerLeft: PropTypes.oneOfType([PropTypes.bool, PropTypes.node]),
  headerRight: PropTypes.oneOfType([PropTypes.bool, PropTypes.node]),
  headerBody: PropTypes.oneOfType([PropTypes.bool, PropTypes.node]),
};

export default HeaderComponent;
