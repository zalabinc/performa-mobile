import { StackNavigator } from 'react-navigation';

import StrategyMapScreen from './screens/StrategyMapScreen';
import PerspectiveScreen from './screens/PerspectiveScreen';
import ObjectiveScreen from './screens/ObjectiveScreen';
import KpiScreen from './screens/KpiScreen';
import InitiativeScreen from './screens/InitiativeScreen';
import ActionPlanScreen from './screens/ActionPlanScreen';


const StrategyMapRouter = StackNavigator({
  StrategyMap: { screen: StrategyMapScreen },
  Perspective: { screen: PerspectiveScreen },
  Objective: { screen: ObjectiveScreen },
  Kpi: { screen: KpiScreen },
  Initiative: { screen: InitiativeScreen },
  ActionPlan: { screen: ActionPlanScreen },
});

export default StrategyMapRouter;
