export default [
  {
    id: 'INITIATIVE-A01',
    name: 'Melakukan proses pelaporan LBU dan pelaporan lainnya',
    measure: 'Telah dilakukan proses pelaporan LBU dan pelaporan lainnya',
  },
  {
    id: 'INITIATIVE-A02',
    name: 'Melakukan smoothing terhadap kewajiban pasca kerja',
    measure: 'Telah dilakukan smoothing terhadap kewajiban pasca kerja',
  },
  {
    id: 'INITIATIVE-A03',
    name: 'Mendorong implementasi Asset Management System',
    measure: 'Terimplementasinya Asset Management System sehingga rekonsiliasi pencatatan aset tetap dan inventari',
  },
  {
    id: 'INITIATIVE-A04',
    name: 'Mengoptimalkan laporan Komitmen dan Kontijensi',
    measure: 'Telah dilakukan pengoptimalan laporan Komitmen dan Kontijensi',
  },
  {
    id: 'INITIATIVE-A05',
    name: 'Mengupayakan seluruh transaksi antar kantor secara online',
    measure: 'Terealisasi seluruh transaksi antar kantor secara online',
  },
  {
    id: 'INITIATIVE-A06',
    name: 'Penyesuaian COA PT Bank Sumut',
    measure: 'Telah dilaksanakan penyesuaian COA PT Bank Sumut atas penerbitan produk-produk baru yang diajukan ol',
  },
  {
    id: 'INITIATIVE-A07',
    name: 'Penyesuaian ketentuan atau kebijakan baru jika ada perubahan dari Bank Indonesia',
    measure: '',
  },
  {
    id: 'INITIATIVE-A08',
    name: 'Telah dilakukan penyesuaian ketentuan atau kebijakan baru jika ada perubahan dari Bank Indonesia',
    measure: '',
  },
  {
    id: 'INITIATIVE-A09',
    name: 'Revisi ketentuan internal terkait penataan administrasi',
    measure: 'Telah dilakukan revisi ketentuan internal terkait penataan administrasi aset tetap dan inventaris la',
  },
  {
    id: 'INITIATIVE-A10',
    name: 'Evaluasi dan rekomendasi lisensi smartnet ',
    measure: 'Terlaksananya evaluasi dan rekomendasi lisensi smartnet Cisco untuk perangkat yang expired bulan D',
  },
];
