export default [
  {
    id: 'SO-A01',
    name: 'Meningkatkan Jumlah dan Kualitas Komposisi Pendanaan',
    description: 'Meningkatkan jumlah dan kualitas komposisi pendanaan giro, tabungan dan deposito.',
    info: '10 KPI and 10 Initiatives',
  },
  {
    id: 'SO-A02',
    name: 'Meningkatnya Nilai Pemegang Saham',
    description: 'Meningkatnya nilai Bank untuk memaksimalkan kesejahteraan pemegang saham dan mendukung pertumbuhan Bank.',
    info: '3 KPI and 22 Initiatives',
  },
  {
    id: 'SO-A03',
    name: 'Pemenuhan Komitmen terhadap Regulator',
    description: 'Meningkatkan kepatuhan terhadap Regulator dengan senantiasa memenuhi Komitmen terhadap Regulator.',
    info: '5 KPI and 11 Initiatives',
  },
  {
    id: 'SO-A04',
    name: 'Meningkatnya Efisiensi Bank',
    description: 'Meningkatnya efisiensi Bank sebagai hasil dari upaya Bank untuk mengelola dana Bank secara optimal.',
    info: '9 KPI and 14 Initiatives',
  },
  {
    id: 'SO-A05',
    name: 'Implementasi Risk Management',
    description: 'Mengimplementasikan pengelolaan risiko Bank dengan akurat dan komprehensif.',
    info: '21 KPI and 18 Initiatives',
  },
  {
    id: 'SO-A06',
    name: 'Pengembangan Jaringan E-Delivery',
    description: 'Mengembangkan jaringan e- delivery dengan menambah jumlah channel e- delivery.',
    info: '13 KPI and 77 Initiatives',
  },
  {
    id: 'SO-A07',
    name: 'Meningkatkan Kualitas Penanganan Kredit Bermasalah',
    description: 'Meningkatkan penanganan kredit bermasalah secara optimal sehingga dapat segera direcovery dan atau direstrukturisasi.',
    info: '11 KPI and 12 Initiatives',
  },
  {
    id: 'SO-A08',
    name: 'Meningkatnya Pendapatan Bunga',
    description: 'Meningkatnya pendapatan yang diperoleh Bank dari bunga.',
    info: '19 KPI and 12 Initiatives',
  },
  {
    id: 'SO-A09',
    name: 'Membangun Sistem Informasi & IT yang mendukung Daya Saing Bank',
    description: 'Membangun sistem informasi dan IT untuk memenuhi kebutuhan operasional dan mendukung daya saing bank.',
    info: '18 KPI and 35 Initiatives',
  },
  {
    id: 'SO-A10',
    name: 'Digital Marketing',
    description: 'Meningkatkan digital marketing dengan mengembangkan fasilitas marketing melalui internet.',
    info: '5 KPI and 9 Initiatives',
  },
];
