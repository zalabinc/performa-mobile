export default [
  {
    id: 'AP-A001',
    name: 'Evaluasi Pengembangan Sistem Perhitungan CKPN',
  },
  {
    id: 'AP-A002',
    name: 'General Audit Tahun Buku 2015',
  },
  {
    id: 'AP-A003',
    name: 'Koordinasi dengan Laporan LPS by system',
  },
  {
    id: 'AP-A004',
    name: 'Membuat MIS untuk Laporan Keuangan kepada Regulator',
  },
  {
    id: 'AP-A005',
    name: 'Membuat Sistem Asset Management',
  },
  {
    id: 'AP-A006',
    name: 'Membuat Sistem Rekening Antar Kantor Online',
  },
  {
    id: 'AP-A007',
    name: 'Mendorong cabang-cabang untuk menggunakan fasilitas HRIS Jimbra untuk penyampaian Laporan',
  },
  {
    id: 'AP-A008',
    name: 'Mengikuti Pelatihan terhadap ketentuan dan pelaporan terbaru',
  },
  {
    id: 'AP-A009',
    name: 'Review Laporan Keuangan Periode Juni 2015',
  },
  {
    id: 'AP-A010',
    name: 'Meningkatkan Kualitas Penyampaian Laporan Keuangan kepada Regulator dan pihak ketiga lainnya',
  },
];
