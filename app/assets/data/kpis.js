export default [
  {
    id: 'KPI2017-A01',
    name: 'Nilai Funding di Jaringan Baru',
    description: 'Nilai Funding di Jaringan Baru adalah Total Nilai Funding di Jaringan (Unit Kantor) Baru',
  },
  {
    id: 'KPI2017-A02',
    name: 'Profit',
    description: '% Ketersediaan kartu ATM pada unit operasional',
  },
  {
    id: 'KPI2017-A03',
    name: 'Nilai Kredit dari Produk/Fitur Baru',
    description: 'Nilai Kredit dari Produk/Fitur Baru adalah Total Nilai Kredit dari Produk/Fitur Baru. Nilai Kredit dari Produk/Fitur Baru = Total Nilai Kredit dari Produk/Fitur Baru.',
  },
  {
    id: 'KPI2017-A04',
    name: '% Program promosi terlaksana sesuai rencana',
    description: '% Program promosi terlaksana sesuai rencana adalah persentase jumlah program promosi yang terlaksana sesuai dengan rencana dibandingkan dengan total program promosi yang direncanakan. % Program promosi terlaksana sesuai rencana = (jumlah program promosi yang terlaksana sesuai dengan rencana / total program promosi yang direncanakan) * 100%.',
  },
  {
    id: 'KPI2017-A05',
    name: 'Nilai Transaksi di Jaringan Baru',
    description: 'Nilai Transaksi di Jaringan Baru adalah Total Nominal Nilai Transaksi di Jaringan (unit kantor) Baru',
  },
  {
    id: 'KPI2017-A06',
    name: '% Kesesuaian dengan aturan akuntansi terbaru',
    description: '% Kesesuaian dengan aturan akuntansi terbaru adalah persentase jumlah aturan akuntansi perusahaan yang sesuai dengan aturan akuntansi terbaru dibandingkan dengan jumlah total aturan akuntansi perusahaan. % Kesesuaian dengan aturan akuntansi terbaru = (jumlah aturan akuntansi perusahaan yang sesuai dengan aturan akuntansi terbaru / jumlah total aturan akuntansi perusahaan) * 100%',
  },
  {
    id: 'KPI2017-A07',
    name: 'Nilai Kredit / Pembiayaan Mikro',
    description: 'Nilai Kredit / Pembiayaan Mikro adalah Total Nilai Kredit /Pembiayaan Mikro (Plafond kredit <= Rp. 50.000.000,- )',
  },
  {
    id: 'KPI2017-A08',
    name: '% Komplain terkait transaksi channeling yang terselesaikan sesuai ketentuan',
    description: '% Komplain terkait transaksi channeling yang terselesaikan sesuai ketentuan adalah persentase jumlah komplain terkait transaksi channeling yang terselesaikan sesuai ketentuan dibandingkan dengan total komplain terkait transaksi channeling. % Komplain terkait transaksi channeling yang terselesaikan sesuai ketentuan= (jumlah komplain terkait transaksi channeling yang terselesaikan sesuai ketentuan/ total komplain terkait transaksi channeling) * 100%.',
  },
  {
    id: 'KPI2017-A09',
    name: 'Collection (Nilai Penagihan Pokok dan Bunga sesuai rencana)',
    description: 'Collection (Nilai Penagihan Pokok dan Bunga sesuai rencana) adalah persentase nilai penagihan pokok dan bunga yang tertagih dibandingkan dengan nilai penagihan pokok dan bunga yang direncanakan. Collection (Nilai Penagihan Pokok dan Bunga sesuai rencana) = (nilai penagihan pokok dan bunga yang tertagih/nilai penagihan pokok dan bunga yang direncanakan) * 100%',
  },
  {
    id: 'KPI2017-A10',
    name: '% Seluruh Program Kerja terlaksana sesuai rencana',
    description: '% Seluruh Program Kerja terlaksana sesuai rencana adalah persentase jumlah program kerja yang terlaksana sesuai rencana dibandingkan dengan total program kerja yang direncanakan. % Seluruh Program Kerja terlaksana sesuai rencana = (jumlah program kerja yang terlaksana sesuai rencana / total program kerja yang direncanakan) * 100%',
  },
];
