export default [
  { id: 1, target: 90, actual: 33, note: 'Target rendah, masih dalam tahap penyesuaian dengan cabang' },
  { id: 2, target: 90, actual: 55, note: 'Peningkatan target untuk penerapan strategy baru' },
  { id: 3, target: 90, actual: 66, note: 'Penyesuaian dengan RTBS' },
  { id: 4, target: 90, actual: 33, note: 'Target rendah, masih dalam tahap penyesuaian dengan cabang' },
  { id: 5, target: 90, actual: 88, note: 'Menunggu sinkronisasi data dari masing masing cabang' },
  { id: 6, target: 90, actual: 90, note: 'Target terpenuhi' },
  { id: 7, target: 90, actual: 89, note: 'Tidak ada catatan' },
  { id: 8, target: 90, actual: 78, note: 'Tidak ada catatan' },
  { id: 9, target: 90, actual: 77, note: 'Tidak ada catatan' },
  { id: 10, target: 90, actual: 56, note: 'Tidak ada catatan' },
  { id: 11, target: 90, actual: 89, note: 'Tidak ada catatan' },
  { id: 12, target: 90, actual: 81, note: 'Tidak ada catatan' },
];