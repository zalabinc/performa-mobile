export default [
  {
    id: 1,
    name: 'Financial',
    description: 'Sasaran yang berhubungan dengan kinerja keuangan Bank untuk meningkatkan nilai perusahaan pada pemegang saham.',
    score: 90,
  },
  {
    id: 2,
    name: 'Customer',
    description: 'Sasaran yang berhubungan dengan kepuasan dan loyalitas nasabah untuk mencapai pertumbuhan nasabah (customer base).',
    score: 45,
  },
  {
    id: 3,
    name: 'Internal Business Processes',
    description: 'Sasaran yang berhubungan dengan perbaikan proses bisnis yang efektif dan efisien, sehingga memberikan kepuasan pada nasabah.',
    score: 67,
  },
  {
    id: 4,
    name: 'Learning and Growth',
    description: 'Sasaran yang berhubungan dengan kapabilitas organisasi dan sumber daya manusia yang mumpuni sehingga mampu menjalankan proses bisnis dengan baik.',
    score: 50,
  },
];
