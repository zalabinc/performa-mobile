import React, { Component } from 'react';

import { DrawerNavigator } from 'react-navigation';
import { StyleProvider } from 'native-base';

import SidebarScreen from './screens/SidebarScreen';
import WelcomeScreen from './screens/WelcomeScreen';
import LoginScreen from './screens/LoginScreen';
import HomeScreen from './screens/HomeScreen';
import CompanyScreen from './screens/CompanyScreen';
// import DashboardScreen from './screens/DashboardScreen';
// import StrategyMapScreen from './screens/StrategyMapScreen';
import MessageScreen from './screens/MessageScreen';
import ChatScreen from './screens/ChatScreen';
import SettingScreen from './screens/SettingScreen';
import PeopleScreen from './screens/PeopleScreen';
import ScoreCardScreen from './screens/ScoreCardScreen';
// import ActionPlanScreen from './screens/ActionPlanScreen';
// import KpiScreen from './screens/KpiScreen';
// import PerspectiveScreen from './screens/PerspectiveScreen';
// import ObjectiveScreen from './screens/ObjectiveScreen';
// import InitiativeScreen from './screens/InitiativeScreen';
import UserScreen from './screens/UserScreen';
// import ProfileScreen from './screens/ProfileScreen/ProfileRouter';

import getTheme from './themes/components';
import material from './themes/variables/material';

import StrategyMapRouter from './StrategyMapRouter';

const AppRouter = DrawerNavigator(
  {
    Welcome: { screen: WelcomeScreen },
    StrategyMap: { screen: StrategyMapRouter },
    // Perspective: { screen: PerspectiveScreen },
    // Objective: { screen: ObjectiveScreen },
    // Initiative: { screen: InitiativeScreen },
    // Kpi: { screen: KpiScreen },
    Profile: { screen: UserScreen },
    // ActionPlan: { screen: ActionPlanScreen },

    Message: { screen: MessageScreen },
    Home: { screen: HomeScreen },
    Company: { screen: CompanyScreen },
    Dashboard: { screen: HomeScreen },
    Chat: { screen: ChatScreen },
    Setting: { screen: SettingScreen },
    Login: { screen: LoginScreen },
    People: { screen: PeopleScreen },
    ScoreCard: { screen: ScoreCardScreen },
  },
  {
    contentComponent: props => <SidebarScreen {...props} />,
  },
);

class App extends Component {
  render() {
    return (
      <StyleProvider style={getTheme(material)}>
        <AppRouter />
      </StyleProvider>

    );
  }
}

export default App;
